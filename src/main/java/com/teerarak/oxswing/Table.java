/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teerarak.oxswing;

import java.io.Serializable;

/**
 *
 * @author AuyouknoW
 */
public class Table implements Serializable{

    char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;
    private int lastcol;
    private int lastrow;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println("");
        }

    }
    public char getRowCol(int row,int col){
        return table[row][col];
    }

    public boolean setRowCol(int row, int col) {
        if(isFinish()){
            return false;
        }
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            checkWin();
            return true;
            

        }
        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    void checkCol() {

        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setstatWinLose();
    }

    private void setstatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkRow() {

        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currentPlayer.getName()) {
                return;
            }

        }
        finish = true;
        winner = currentPlayer;
        setstatWinLose();
    }
    void checkLeft() {
        for (int col = 0; col < 3; col++) {
            if (table[col][col] != currentPlayer.getName()) {
                return;
            }

        }
        finish = true;
        winner = currentPlayer;
    }
     void checkRight() {
        int R = 2;
        for (int row = 0; row < 3; row++, R--) {
            if (table[row][R] != currentPlayer.getName()) {
                return;
            }

        }
        finish = true;
        winner = currentPlayer;
    }

   


    public void checkWin() {
        checkRow();
        checkCol();
       checkLeft();
       checkRight();
    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;
    }
}

